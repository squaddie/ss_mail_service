<?php


namespace App\Services;


use App\Jobs\VerifySuspiciousDomains;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Filters
{
    const FILE_CSV = '.csv';
    const FILE_TXT = '.txt';

    public $trusted_domains = [
        'gmail', 'aol.com', 'outlook', 'hotmail.com', 'msn', 'zoho', 'mail.com', 'yahoo', 'proton', 'icloud', 'gmx', 'yandex', 'ya', 'rambler'
    ];

    public $file;
    public $fileType;

    public function __construct($file)
    {
        $this->file = $file;
        $this->fileType = File::extension(storage_path('app/'.$this->file));
    }

    public function getTrustedDomains(): array
    {
        return $this->trusted_domains;
    }

    public function parseTextFile()
    {
        $source = file_get_contents(storage_path('app/'.$this->file));

        return explode("\n", $source);
    }

    public function parseCsvFile()
    {
        $source = file_get_contents(public_path($this->file));
        $source = explode("\n", $source);

        $emails = [];
        foreach ($source as $dataset) {
            foreach (array_filter(explode(',', $dataset)) as $string) {
                $string = str_replace('"', "", $string);
                $string = str_replace("'", "", $string);
                if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
                    $emails[] = $string;
                }

            }
        }

        return $emails;
    }

    /**
     * Generate an collection of emails from the given source.
     *
     * @param string $file
     * @return Collection
     */
    public function getUniqueEmailsList(): Collection
    {
        if ($this->fileType == self::FILE_CSV)
            $source = $this->parseCsvFile();
        else
            $source = $this->parseTextFile();

        $collection = new Collection();
        foreach ($source as $email)
            $collection->push(trim(rtrim(strtolower($email))));

        return $collection;
    }

    /**
     * Get the collection of emails where domains is in trusted list.
     *
     * @return Collection
     */
    public function getTrustedEmails(): Collection
    {
        $emails = $this->getUniqueEmailsList();

        return $emails
            ->filter(function ($row, $key) {
                foreach ($this->getTrustedDomains() as $domain)
                    if (Str::contains($row, $domain))
                        return true;
                return false;
            })
            ->unique();
    }

    /**
     * Get the collection of emails where domains is not in trusted list.
     *
     * @return Collection
     */
    public function getSuspiciousEmails(): Collection
    {
        $emails = $this->getUniqueEmailsList();

        return $emails
            ->filter(function ($row, $key) {
                foreach ($this->getTrustedDomains() as $domain)
                    if (Str::contains($row, $domain))
                        return false;
                return true;
            })
            ->unique();
    }

    /**
     * Get the collection of suspicious domains from the give source.
     *
     * @return Collection
     */
    public function getUniqueSuspiciousDomains(): Collection
    {
        $emails = $this->getSuspiciousEmails();

        return $emails
            ->map(function ($row, $key) {
                $array = explode('@', $row);
                return end($array);
            })
            ->unique()
            ->values();
    }

    public function verifySuspiciousDomains()
    {
        $domains = $this->getUniqueSuspiciousDomains();
        VerifySuspiciousDomains::dispatch($domains, Auth::id());
    }

    public function index()
    {
        dd($this->parseCsvFile());

        $trusted = $this->getTrustedEmails();
        $suspicious = $this->getSuspiciousEmails();
        $suspiciousDomains = $this->getUniqueSuspiciousDomains();
        dd($suspiciousDomains);
        dd(1);
    }
}
