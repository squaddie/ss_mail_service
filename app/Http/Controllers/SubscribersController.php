<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\EmailsList;
use App\Services\Filters;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    const HTTP_200 = 200;
    const HTTP_500 = 500;

    public $per_page = 50;
    public $title;
    public $filter;
    public $file;

    public function index()
    {
        $lists = EmailsList::withCount('emails')->latest()->paginate($this->per_page);

        return view('subscribers.all', [
            'lists' => $lists
        ]);
    }

    /**
     * @param $emails
     * @param $id
     */
    public function saveEmails($emails, $id)
    {
        $data = [];
        foreach ($emails as $key => $email) {
            $data[$key]['email'] = $email;
            $data[$key]['list_id'] = $id;
            $data[$key]['created_at'] = now();
        }

        foreach (array_chunk($data, 1000) as $chunk)
            Email::insert($chunk);
    }

    public function createEmailsList($title)
    {
        return EmailsList::create(['title' => $title]);
    }

    public function create(Request $request)
    {
        $userFile = $request->file('file')->store('files');

        // New instance of Filters class
        $this->filter = new Filters($userFile);

        // Generate title when it's not present
        $title = $request->get('title') ?: uniqid();
        $validate = !$request->has('validate') ? false : true;

        if ($validate)
            $emails = $this->filter->getTrustedEmails();
        else
            $emails = $this->filter->getUniqueEmailsList();

        $this->saveEmails($emails, $this->createEmailsList($title)->id);

        return response([], self::HTTP_200);
    }
}
