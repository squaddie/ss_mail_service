<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\EmailsList;
use App\Models\Message;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use Mailgun\Mailgun;

class NewsletterController extends Controller
{
    public function index(Newsletter $newsletter, EmailsList $emailsList)
    {
        $data = [
            'newsletters' => $newsletter->getNewsletters(),
            'lists' => EmailsList::withCount('emails')->latest()->get()
        ];

        return view('newsletters.all', $data);
    }

    /**
     * Send emails
     *
     * @param $emails
     * @param $json
     * @param $view
     */
    public function mailgunInstance($emails, $json, $view)
    {
        $mailgun = Mailgun::create(env('MAILGUN_SECRET'), env('MAILGUN_ENDPOINT'));
        $mailgun->messages()->send(env('MAILGUN_DOMAIN'), [
            'from' => env('MAIL_FROM'),
            'to' => $emails,
            'subject' => 'Sincere Systems Group',
            'html' => $view,
            'recipient-variables' => $json,
            'o:tracking-opens' => 'yes',
            'o:tracking-clicks' => 'yes',
            'v:message_id' => '%recipient.message_id%',
        ]);
    }

    public function buildMessages($newsletter_id, $emails)
    {
        $result = [];
        foreach ($emails as $key => $email) {
            $result[$key]['email_id'] = $email->id;
            $result[$key]['newsletter_id'] = $newsletter_id;
            $result[$key]['list_id'] = $email->list_id;
            $result[$key]['created_at'] = now();
        }

        return $result;
    }

    public function addMessages($data)
    {
        foreach (array_chunk($data, 1000) as $chunk)
            Message::insert($chunk);
    }

    public function create(Request $request, Email $emails, Newsletter $newsletter)
    {
        $view = view('mail.html')->render();
        $list = $emails->doesntHave('blockedEmail')->where('list_id', $request->id)->get();
        $newsletter_id = $newsletter->create($request->all())->id;

        $messages = $this->buildMessages($newsletter_id, $list);
        $this->addMessages($messages);

        Message::with('email')
            ->where('newsletter_id', $newsletter_id)
            ->chunkById(1000, function ($messages) use ($view) {
                $emails = [];
                $json = [];
                foreach ($messages as $key => $messsage) {
                    $json[$messsage->email->email] = ['message_id' => $messsage->id];
                    $emails[] = $messsage->email->email;
                }
                $json = json_encode($json);

                $this->mailgunInstance($emails, $json, $view);
            });
    }
}
