<?php

namespace App\Http\Controllers;

use App\Models\EmailsList;
use App\Models\Email;
use App\Services\Filters;

class FilterController extends Controller
{
    public $filter;
    public $file;
    public $title;

    public function __construct($filter)
    {
        $this->filter = new Filters($this->file);
    }

    public function saveEmails($emails, $id)
    {
        $data = [];
        foreach ($emails as $key => $email) {
            $data[$key]['email'] = $email;
            $data[$key]['list_id'] = $id;
            $data[$key]['created_at'] = now();
        }

        foreach (array_chunk($data, 1000) as $chunk)
            Email::insert($chunk);
    }

    public function createEmailsList()
    {
        return EmailsList::create(['title' => $this->title]);
    }
}
