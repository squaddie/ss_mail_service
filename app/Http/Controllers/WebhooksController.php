<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

//TODO Verify mailgun request

class WebhooksController extends Controller
{
    const HTTP_200 = 200;
    const HTTP_500 = 500;

    public function delivered(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                Message::where('id', $event['event-data']['user-variables']['message_id'])->update([
                    'delivered' => true
                ]);

                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }

    public function click(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                Message::where('id', $event['event-data']['user-variables']['message_id'])->update([
                    'clicked' => true
                ]);

                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }

    public function open(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                Message::where('id', $event['event-data']['user-variables']['message_id'])->update([
                    'opened' => true
                ]);

                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }

    public function failPermanent(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                $message = Message::where('id', $event['event-data']['user-variables']['message_id'])->first();
                $message->permanent_fail = true;
                $message->temporary_fail = true;
                $message->save();

                BlackList::updateOrCreate([
                    'email' => $message->email->email
                ]);


                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }

    public function failTemporary(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                Message::where('id', $event['event-data']['user-variables']['message_id'])->update([
                    'temporary_fail' => true
                ]);

                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }

    public function spam(Request $request)
    {
        $event = $request->all();
        if (isset($event['event-data']['user-variables']['message_id'])) {
            try {
                Message::where('id', $event['event-data']['user-variables']['message_id'])->update([
                    'spam' => true
                ]);

                return response('', self::HTTP_200);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                Log::error($exception->getTraceAsString());

                return response('', self::HTTP_500);
            }
        }

        return response('', self::HTTP_500);
    }
}
