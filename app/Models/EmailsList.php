<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasMany;

class EmailsList extends Model
{
    use HasFactory;

    protected $table = 'emails_lists';
    protected $fillable = [
        'title'
    ];

    public function emails(): HasMany
    {
        return $this->hasMany(Email::class, 'list_id', 'id')->doesntHave('blockedEmail');
    }
}
