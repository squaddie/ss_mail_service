<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Newsletter extends Model
{
    use HasFactory;

    protected $table = 'newsletters';
    protected $fillable = [
        'title'
    ];

    public function getNewsletters()
    {
        return $this->addCounts()->latest()->get();
    }

    public function scopeAddCounts($query)
    {
        $query->withCount('messages');
        $query->withCount('deliveredMessages');
        $query->withCount('openedMessages');
        $query->withCount('clickedMessages');
        $query->withCount('complaintMessages');
        $query->withCount('permanentlyFailedMessages');
        $query->withCount('temporaryFailedMessages');

        return $query;
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id');
    }

    public function deliveredMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('delivered', true);
    }

    public function openedMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('opened', true);
    }

    public function clickedMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('clicked', true);
    }

    public function complaintMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('spam', true);
    }

    public function permanentlyFailedMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('permanent_fail', true);
    }

    public function temporaryFailedMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'newsletter_id', 'id')->where('temporary_fail', true);
    }

}
