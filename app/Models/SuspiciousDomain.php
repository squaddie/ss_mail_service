<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuspiciousDomain extends Model
{
    use HasFactory;

    protected $table = 'suspicious_domains';
    protected $fillable = [
        'domain'
    ];
}
