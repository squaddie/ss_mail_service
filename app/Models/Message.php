<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasOne;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';
    protected $fillable = [
        'email_id',
        'newsletter_id',
        'list_id',
        'delivered',
        'opened',
        'clicked',
        'spam',
        'temporary_fail',
        'permanent_fail',
    ];

    public function email(): HasOne
    {
        return $this->hasOne(Email::class, 'id', 'email_id');
    }
}
