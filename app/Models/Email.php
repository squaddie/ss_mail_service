<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Email extends Model
{
    use HasFactory;

    protected $table = 'emails';
    protected $fillable = [
        'email',
        'list_id',
    ];

    public function blockedEmail(): HasOne
    {
        return $this->hasOne(BlackList::class, 'email', 'email');
    }
}
