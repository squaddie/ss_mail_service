<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SubscribersController;
use App\Http\Controllers\NewsletterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth:web']], function () {
    Route::get('/newsletters-all', [NewsletterController::class, 'index'])->name('newsletters.all');
    Route::post('/newsletters-add', [NewsletterController::class, 'create'])->name('newsletters.add');

    Route::get('/subscribers-all', [SubscribersController::class, 'index'])->name('subscribers.all');
    Route::post('/subscribers-add', [SubscribersController::class, 'create'])->name('subscribers.add');

});


Auth::routes();
Route::get('/', function () {
    return redirect('login');
});

Route::get('/register', function () {
    return false;
});
