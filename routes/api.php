<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebhooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'mailgun'], function () {
    Route::post('delivered', [WebhooksController::class, 'delivered']);
    Route::post('click', [WebhooksController::class, 'click']);
    Route::post('open', [WebhooksController::class, 'open']);
    Route::post('fail-permanent', [WebhooksController::class, 'failPermanent']);
    Route::post('fail-temporary', [WebhooksController::class, 'failTemporary']);
    Route::post('spam', [WebhooksController::class, 'spam']);
});



