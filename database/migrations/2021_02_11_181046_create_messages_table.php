<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('email_id');
            $table->unsignedBigInteger('newsletter_id');
            $table->unsignedBigInteger('list_id');
            $table->boolean('delivered')->default(false);
            $table->boolean('opened')->default(false);
            $table->boolean('clicked')->default(false);
            $table->boolean('spam')->default(false);
            $table->boolean('temporary_fail')->default(false);
            $table->boolean('permanent_fail')->default(false);
            $table->timestamps();

            $table->index('email_id');
            $table->index('newsletter_id');
            $table->index('list_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
