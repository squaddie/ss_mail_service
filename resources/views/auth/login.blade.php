@extends('layouts.auth')

@section('content')

    <div>
        <h3>Почтовый сервис</h3>
        <p class="text-muted">Авторизация</p>
        <form class="m-t" method="POST" action="{{ route('login') }}">
                @csrf
            <div class="form-group row">
                <div class="col-md-12">
                    <input id="email" placeholder="Логин" type="email" class="form-control input-sm @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <input id="password" placeholder="Пароль" type="password" class="form-control input-sm @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-12">
                    <button type="submit" class="btn d-block w-full btn-sm btn-primary">
                        {{ __('auth.login_button') }}
                    </button>
                </div>
            </div>
        </form>
    </div>


@endsection
