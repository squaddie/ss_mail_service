<div class="modal inmodal" id="add-subscribers" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <small class="font-bold">
                    Предположительно, тут будет порядка 10% погрешности в качестве фильтрации.
                </small>
            </div>

            <form action="" id="create-subscribers-form">

                <div class="modal-body">
                    <div class="form-group">
                        <label>Название списка</label>
                        <input type="text" name="title" placeholder="" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Список. Файл должен быть в формате .CSV или .TXT</label>
                        <input type="file" name="file" placeholder="" class="">
                    </div>
                    <div class="form-group">
                        <label class="checkbox-inline" for="validate-checkbox">
                            <input type="checkbox" name="validate" id="validate-checkbox" checked> Проверять валидность
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-primary">Создать</button>
                </div>

            </form>

        </div>
    </div>
</div>
