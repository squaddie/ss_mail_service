@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Списоки адресов</h5>
                        <div class="ibox-tools">
                            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-subscribers">Добавить новый список</a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" placeholder="Поиск" class="input-sm form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Искать!</button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="project-list">

                            <table class="table table-hover">
                                <tbody>
                                @foreach($lists as $list)
                                    <tr>
                                        <td class="project-status" width="15%">
                                            <span class="label label-primary">
                                                Адресов: {{ $list->emails_count }}
                                            </span>
                                        </td>
                                        <td class="project-title">
                                            <a href="#">{{ $list->title }}</a>
                                            <br>
                                            <small>Дата создания {{ $list->created_at->format('d.m.Y') }}</small>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('subscribers.modals.add')
@endsection
@section('js')
    <script>
        $(document).ready(function () {

            $('#create-subscribers-form').on('submit', function (e) {
                let form = $(this);
                let body = $('body');
                body.addClass('body-progress');
                toastr.info('Загружаем и парсим файл, нужно подождать. Ты увидишь уведомление когда я все закончу.', 'Работаем...');
                e.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: '{{ route('subscribers.add') }}',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        toastr.success('Файлик загружен, дай мне еще 2 секундочки...', 'Наработались...');
                        setTimeout(location.reload(), 1200);
                    },
                    error: function () {
                        toastr.error('Что-то сломалось, возможно проблемный файлик?', 'Ууупс...');
                    }
                });
            });

        });
    </script>
@endsection
