<div class="modal inmodal" id="add-newsletter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <form action="" id="create-newsletter-form">

                <div class="modal-body">
                    <div class="form-group">
                        <label>Название рассылки</label>
                        <input type="text" name="title" placeholder="" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Список адресов</label>
                        <select name="id" id="" class="form-control form-control-sm">
                            @foreach($lists as $list)
                                <option value="{{ $list->id }}">{{ $list->title }} ( {{ $list->emails_count }} )</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-xs btn-primary">Создать</button>
                </div>

            </form>

        </div>
    </div>
</div>
