@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Рассылки</h5>
                        <div class="ibox-tools">
                            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-newsletter">Создать рассылку</a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="project-list">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Messages</th>
                                        <th>Delivered</th>
                                        <th>Bounce</th>
                                        <th>Opened</th>
                                        <th>Clicked</th>
                                        <th>Complained</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($newsletters as $list)
                                    <tr>
                                        <td class="project-title">
                                            <a href="#">{{ $list->title }}</a>
                                            <br>
                                            <small>Дата создания {{ $list->created_at->format('d.m.Y h:i') }}</small>
                                        </td>
                                        <td>
                                            <b>{{ $list->messages_count }}</b>
                                        </td>
                                        <td>
                                            {{ $list->delivered_messages_count }}
                                        </td>
                                        <td>
                                            <b>{{ $list->permanently_failed_messages_count }}</b>
                                        </td>
                                        <td>
                                            {{ $list->opened_messages_count }}
                                        </td>
                                        <td>
                                            {{ $list->clicked_messages_count }}
                                        </td>
                                        <td>
                                            {{ $list->complaint_messages_count }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('newsletters.modals.add', ['lists' => $lists])
@endsection
@section('js')
    <script>
        $(document).ready(function () {

            $('#create-newsletter-form').on('submit', function (e) {
                let form = $(this);
                let body = $('body');
                body.addClass('body-progress');
                toastr.info('Загружаем и парсим файл, нужно подождать. Ты увидишь уведомление когда я все закончу.', 'Работаем...');
                e.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: '{{ route('newsletters.add') }}',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        toastr.success('Письма отправлены, дай мне еще 2 секундочки...', 'Наработались...');
                        setTimeout(location.reload(), 1200);
                    },
                    error: function () {
                        toastr.error('Что-то сломалось, попробуй перегазгрузить страничку!', 'Ууупс...');
                    }
                });
            });

        });
    </script>
@endsection
